<?php
/**
 * @file
 * Contains ResourceUIController class.
 */

class ResourceUIController extends EntityDefaultUIController {

  /**
   * {@inheritdoc}
   */
  public function hook_menu() {
    $items = array();

    $items['resource/add'] = array(
      'title' => 'Add a Resource',
      'description' => 'Add a new Resource',
      'page callback' => 'resource_add_page',
      'access callback' => 'user_is_logged_in',
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'resource.admin.inc',
      'file path' => drupal_get_path('module', 'resource_library'),
    );

    $items['resource/add/%resource_type'] = array(
      'title callback' => 'resource_add_page_title',
      'title arguments' => array(2),
      'page callback' => 'resource_add_form_wrapper',
      'page arguments' => array(2),
      'access callback' => 'resource_access',
      'access arguments' => array('create', 2),
      'file' => 'resource.admin.inc',
      'file path' => drupal_get_path('module', 'resource_library')
    );

    if (module_exists('ctools')) {
      $items['resource/add/%resource_type/%ctools_js'] = array(
        'title callback' => 'resource_add_page_title',
        'title arguments' => array(2),
        'page callback' => 'resource_add_form_wrapper',
        'page arguments' => array(2, 3),
        'access callback' => 'resource_access',
        'access arguments' => array('create', 2),
        'file' => 'resource.admin.inc',
        'file path' => drupal_get_path('module', 'resource_library')
      );
    }

    $items['resource/%resource'] = array(
      'title callback' => 'entity_label',
      'title arguments' => array('resource', 1),
      'page callback' => 'resource_page_view',
      'page arguments' => array(1),
      'access callback' => 'resource_access',
      'access arguments' => array('view', 1),
      'file' => 'resource.ui.inc',
      'file path' => drupal_get_path('module', 'resource_library'),
    );

    $items['resource/%resource/view'] = array(
      'title' => 'View',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );

    $items['resource/%resource/edit'] = array(
      'title' => 'Edit',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('resource_form', 1),
      'access callback' => 'resource_access',
      'access arguments' => array('edit', 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'type' => MENU_LOCAL_TASK,
      'file' => 'resource.admin.inc',
      'file path' => drupal_get_path('module', 'resource_library'),
    );

    $items['resource/%resource/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('resource_delete_form', 1),
      'access callback' => 'resource_access',
      'access arguments' => array('delete', 1),
      'weight' => 1,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'type' => MENU_LOCAL_TASK,
      'file' => 'resource.admin.inc',
      'file path' => drupal_get_path('module', 'resource_library'),
    );

    return $items;
  }
}

/**
 * Page callback for resources.
 */
function resource_page_view($resource) {
  $controller = entity_get_controller('resource');
  $content = $controller->view(array($resource->id => $resource));
  return $content;
}
