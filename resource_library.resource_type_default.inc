<?php
/**
 * @file
 * Contains 3 base resource types.
 */

/**
 * Implements hook_default_resource_type().
 */
function resource_library_default_resource_type() {
  $types = array();

  $types['download'] = entity_import('resource_type', '{ "type" : "download", "label" : "Download", "data" : [], "rdf_mapping" : [] }');
  $types['audio'] = entity_import('resource_type', '{ "type" : "audio", "label" : "Audio", "data" : [], "rdf_mapping" : [] }');
  $types['video'] = entity_import('resource_type', '{ "type" : "video", "label" : "Video", "data" : [], "rdf_mapping" : [] }');

  return $types;
}
