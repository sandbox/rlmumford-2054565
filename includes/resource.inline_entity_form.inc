<?php
/**
 * @file
 * The Inline Entity Form controller for Resources.
 */

class ResourceInlineEntityFormController extends EntityInlineEntityFormController {

  /**
   * {@inheritdoc}
   */
  public function labels() {
    return array(
      'singular' => t('resource'),
      'plural' => t('resources'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function entityForm($entity_form, &$form_state) {
    $entity_form = parent::entityForm($entity_form, $form_state);
    $resource = $entity_form['#entity'];

    $entity_form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#required' => TRUE,
      '#default_value' => $resource->title,
      '#maxlength' => 255,
      // The label might be missing if the Title module has replaced it.
      '#weight' => -5,
    );

    // Only show title, icon and file fields.
    foreach(element_children($entity_form) as $key) {
      if(empty($entity_form[$key][LANGUAGE_NONE]['#field_name'])) {
        continue;
      }

      $field_name = $entity_form[$key][LANGUAGE_NONE]['#field_name'];
      if (in_array($field_name, array('resource_icon', 'resource_file'))) {
        continue;
      }

      $entity_form[$key]['#access'] = FALSE;
    }

    drupal_alter('inline_resource_form', $entity_form, $form_state);

    return $entity_form;
  }

  /**
   * {@inheritdoc}
   */
  public function entityFormSubmit($entity_form, &$form_state) {
    global $user;

    parent::entityFormSubmit($entity_form, $form_state);

    $resource = $entity_form['#entity'];
    $resource->uid = $user->uid;
    if (!empty($resource->is_new)) {
      $resource->created = time();
    }
    $resource->changed = time();
    $resource->timestamp = time();
    $resource->status = TRUE;
  }

}
