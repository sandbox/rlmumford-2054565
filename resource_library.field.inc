<?php
/**
 * @file
 * Contains field type definition.
 */

/**
 * Implements hook_field_info().
 */
function resource_library_field_info() {
  return array(
    'resource_reference' => array(
      'label' => t('Resource Reference'),
      'description' => t('This field stores a reference to resources that are part of another resource.'),
      'settings' => array(),
      'instance_settings' => array(
        'allowed_labels' => array(),
      ),
      'default_widget' => 'resource_reference_generic',
      'default_formatter' => 'resource_reference_default',
    ),
  );
}

/**
 * Implements hook_field_instance_settings_form().
 */
function resource_library_field_instance_settings_form($field, $instance) {
  $allowed_labels = $instance['settings']['allowed_labels'];

  $form['allowed_labels'] = array(
    '#type' => 'textarea',
    '#title' => t('Allowed Labels'),
    '#rows' => 10,
    '#description' => t('Each sub-resource can be labelled with its own label. To limit what labels are allowed enter the allowed values into the box, one per line in the format machine_name|label. For example, a seminar may have an Audio Recording, Some Notes and Slides as sub resources in which case you would enter:<br />audio|Audio<br />notes|Notes<br />slides|Slides<br />. It is advisable to use the advanced widget when setting this value.'),
    '#default_value' => list_allowed_values_string($allowed_labels),
    '#element_validate' => array('resource_library_allowed_labels_element_validate'),
  );
}

/**
 * Validate function fo the allowed labels box.
 */
function resource_library_allowed_labels_element_validate($element, &$form_state) {
  $values = list_extract_allowed_values($element['#value'], 'list_text', FALSE);

  if (!is_array($values)) {
    form_error($element, t('Allowed labels list: invalid input.'));
  }

  foreach ($values as $key => $value) {
    if ( drupal_strlen($key) > 255) {
      form_error($element, t('Allowed labels list: each key must be a string at most 255 characters long.'));
      break;
    }
  }

  form_set_value($element, $values, $form_state);
}

/**
 * Implements hook_field_widget_info().
 */
function resource_library_field_widget_info() {
  return array(
    'resource_reference_generic' => array(
      'label' => t('Generic Resource Reference'),
      'field types' => array('resource_reference'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
    'resource_reference_specific' => array(
      'label' => t('Resource Reference'),
      'field types' => array('resource_reference'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function resource_library_field_widget_form(&$form, &$form_State, $field, $instance, $langcode, $items, $delta, $element) {
  $type = $instance['widget']['type'];
  $allowed_labels = $instance['settings']['allowed_labels'];

  switch ($type) {
    case 'resource_reference_generic':
      if (empty($allowed_labels)) {
        $element['label'] = array(
          '#type' => 'textfield',
          '#title' => t('Label'),
          '#description' => t('The label of this resource withing the main resource.'),
          '#default_value' => $items[$delta]['label'],
        );
      }
      else {
        $element['label'] = array(
          '#type' => 'select',
          '#options' => $allowed_labels,
          '#title' => t('Label'),
          '#description' => t('The label of this resource within the main resource.'),
          '#default_value' => isset($items[$delta]['label']) ? $items[$delta]['label'] : NULL,
        );
      }

      $element['resource_id'] = array(
        '#type' => 'value',
        '#value' => isset($items[$delta]['resource_id']) ? $items[$delta]['resource_id'] : NULL,
      );

      $element['resource'] = array(
        '#type' => 'container',
        '#resource' => isset($items[$delta]['resource_id']) ? entity_load_single($items[$delta]['resource_id']

      $element['resource']['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#description' => t('The title of this resource.'),
        '#default_value' => $items[$delta]['label'],
      );


  }
}
