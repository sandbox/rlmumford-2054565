<?php
/**
 * @file
 * Contains tokens integration for our access list content type.
 */

/**
 * Implements hook_tokens().
 */
function resource_library_access_tokens($type, $tokens, array $data = array(), array $options = array()) {
  if ($type != 'resource_access_list') {
    return;
  }

  if (!isset($data['resource_access_list'])) {
    return;
  }

  $replacements = array();
  $access_list = $data['resource_access_list'];
  foreach ($tokens as $name => $original) {
    dpm($name, $original);
    $replacements[$original] = $access_list[$name];
  }

  return $replacements;
}
