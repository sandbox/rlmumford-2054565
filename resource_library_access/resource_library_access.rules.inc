<?php
/**
 * Contains resource library access integration with rules.
 */

/**
 * Implements hook_rules_data_info().
 */
function resource_library_access_rules_data_info() {
  $data_types = array();

  $access_list_properties = array();
  foreach (field_info_instances('resource') as $bundle => $instances) {
    foreach ($instances as $field_name => $instance) {
      $property = array(
        'type' => 'boolean',
        'label' => $instance['label'],
        'setter callback' => 'entity_property_verbatim_set',
      );
      $access_list_properties[$field_name] = $property;
    }
  }

  $data_types['resource_access_list'] = array(
    'label' => t('Resource Field Access List'),
    'wrap' => TRUE,
    'property info' => $access_list_properties,
  );

  return $data_types;
}
