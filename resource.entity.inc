<?php
/**
 * @file
 * Contains Resource Class.
 */

/**
 * Class to model resource entities.
 */
class Resource extends Entity {
  public $id;
  public $revision_id;
  public $type;
  public $title;
  public $uid;
  public $status;
  public $created;
  public $changed;

  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'resource/' . $this->id);
  }
}
