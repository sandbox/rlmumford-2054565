<?php
/**
 * @file
 * Contains forms and other admin pages.
 */

/**
 * Resource add page title callback.
 */
function resource_add_page_title($resource_type) {
  return t('Add @resource', array('@resource' => $resource_type->label));
}

/**
 * Resource add page callback.
 */
function resource_add_form_wrapper($resource_type, $js = FALSE) {
  $resource = entity_create('resource', array('type' => $resource_type->type));
  if (!$js) {
    return drupal_get_form('resource_form', $resource);
  }
  else {
    ctools_include('ajax');
    ctools_include('modal');
    $form_state = array(
      'title' => resource_add_page_title($resource_type),
      'ajax' => TRUE,
      'build_info' => array(
        'args' => array($resource),
      ),
    );

    $commands = ctools_modal_form_wrapper('resource_form', $form_state);
    if (!empty($form_state['executed'])) {
      $commands = array();
      $commands[] = ctools_ajax_command_reload();
    }

    $output = array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
    ajax_deliver($output);
    drupal_exit();
  }
}

/**
 * Resource form.
 */
function resource_form($form, &$form_state, $resource) {
  $form['#resource'] = $resource;
  $form['#is_new'] = !empty($resource->is_new);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $resource->title,
    '#maxlength' => 255,
    // The label might be missing if the Title module has replaced it.
    '#weight' => -5,
  );

  field_attach_form('resource', $resource, $form, $form_state, LANGUAGE_NONE);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Validate callback for resource edit form.
 */
function resource_form_validate($form, &$form_state) {
  $psuedo_entity = (object) $form_state['values'];
  $psuedo_entity->type = $form['#resource']->type;
  field_attach_form_validate('resource', $psuedo_entity, $form, $form_state);
}

/**
 * Submit callback for resource edit form.
 */
function resource_form_submit($form, &$form_state) {
  global $user;

  $resource = $form['#resource'];
  $resource->title = htmlspecialchars($form_state['values']['title']);
  field_attach_submit('resource', $resource, $form, $form_state);

  $resource->uid = $user->uid;
  if (!empty($resource->is_new)) {
    $resource->created = time();
  }
  $resource->changed = time();
  $resource->timestamp = time();
  $resource->status = TRUE;

  entity_save('resource', $resource);

  $form_state['redirect'] = 'resource/' . $resource->id;
}

/**
 * Resource delete form.
 */
function resource_delete_form($form, &$form_state, $resource) {
  $form['#resource'] = $resource;

  $form = confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $resource->title)),
    '',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for the resource delete form.
 */
function resource_delete_form_submit($form, &$form_state) {
  $resource = $form['#resource'];

  entity_delete('resource', $resource->id);
  drupal_set_message(t('The resource %title has been deleted.', array('%title' => $resource->title)));
  watchdog('resource', 'Deleted resource %title.', array('%title' => $resource->title));
  $form_state['redirect'] = 'admin/content/resources';
}

/**
 * Resource add page.
 */
function resource_add_page() {
  $types = resource_get_types();

  $items = array();
  foreach ($types as $type) {
    if (!resource_access('create', $type)) {
      continue;
    }

    $items[] = array(
      'title' => $type->label,
      'href' => 'resource/add/' . $type->type,
      'description' => t('Add a @label resource.', array('@label' => $type->label)),
    );
  }

  if (count($items) == 1) {
    $item = reset($items);
    drupal_goto($items['href']);
  }

  return theme('resource_add_list', array('content' => $items));
}

/**
 * Theme callback for the resource add list.
 */
function theme_resource_add_list($variables) {
  $content = $variables['content'];
  $output = '';

  if ($content) {
    $output = '<dl class="resource-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    $output = '<p>' . t('You have not created any resource types yet. Go to the <a href="@create-type">resource type creation page</a> to add a new resource type.', array('@create-content' => url('admin/structure/resources/add'))) . '</p>';
  }
  return $output;
}


