<?php
/**
 * @file
 * Contains administatration UI functon for resource types.
 */

/**
 * Form Callback to edit a resource type.
 */
function resource_type_form($form, &$form_state, $resource_type, $op = 'edit') {
  if ($op == 'clone') {
    $resource_type->label .= ' (cloned)';
    $resource_type->type = '';
  }

  $form['#resource_type'] = $resource_type;

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $resource_type->label,
    '#description' => t('The human-readable name of this resource type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($resource_type->type) ? $resource_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $resource_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'resource_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this resource type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['#sub_resources'] = !empty($resource_type->resources) ? array_values($resource_type->resources) : array();
  $form_state['sub_resource_count'] = !isset($form_state['sub_resource_count']) ? count($form['#sub_resources']) : $form_state['sub_resource_count'];

  $sub_resource_defaults = array(
    'machine_name' => '',
    'label' => '',
    'type' => '',
    'cardinality' => 1,
  );

  $types = resource_get_types();
  $type_options = array();
  foreach ($types as $name => $info) {
    $type_options[$name] = $info->label;
  }

  $cardinality_options = array(
    FIELD_CARDINALITY_UNLIMITED => t('Unlimited'),
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    5 => 5,
    6 => 6,
    7 => 7,
    8 => 8,
    9 => 9,
    10 => 10,
    11 => 11,
    12 => 12,
  );

  // Form element for the addition of sub resources.
  $form['sub_resources'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sub Resources'),
    '#description' => t('Please enter the settings for any sub resources.'),
    '#tree' => TRUE,
    '#prefix' => '<div id="resource-type-sub-resources-wrapper">',
    '#suffix' => '</div>',
  );

  for ($delta = 0; $delta < $form_state['sub_resource_count']; $delta++) {
    $sub_resource = isset($form['#sub_resources'][$delta]) ? $form['#sub_resources'][$delta] : $sub_resource_defaults;

    $item = array(
      '#type' => 'container',
    );

    $item['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#default_value' => $sub_resource['label'],
      '#required' => TRUE,
      '#size' => 20,
    );

    $item['machine_name'] = array(
      '#type' => 'machine_name',
      '#default_value' => $sub_resource['machine_name'],
      '#maxlength' => 23,
      '#disabled' => !empty($sub_resource['machine_name']),
      '#machine_name' => array(
        'exists' => 'resource_type_sub_resource_exists',
        'source' => array('sub_resources', $delta, 'label'),
      ),
      '#element_validate' => array(
        'resource_type_sub_resource_machine_name_validate',
      ),
    );

    $item['type'] = array(
      '#type' => 'select',
      '#title' => t('Resource Type'),
      '#options' => $type_options,
      '#default_value' => $sub_resource['type'],
      '#required' => TRUE,
      '#disabled' => !empty($sub_resource['machine_name']),
    );

    $item['cardinality'] = array(
      '#type' => 'select',
      '#title' => t('No. Available'),
      '#description' => t('How many of this sub resource can a resource of this type have?'),
      '#default_value' => $sub_resource['cardinality'],
      '#required' => TRUE,
      '#options' => $cardinality_options,
      '#disabled' => !empty($sub_resource['machine_name']),
    );

    $item['delete'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove'),
      '#access' => !empty($sub_resource['machine_name']),
    );

    $form['sub_resources'][$delta] = $item;
  }

  $form['sub_resources']['_add_another'] = array(
    '#type' => 'submit',
    '#value' => t('Add Sub Resource'),
    '#limit_validation_errors' => array(
      array('sub_resources'),
    ),
    '#submit' => array(
      'resource_type_add_another_submit',
    ),
    '#ajax' => array(
      'callback' => 'resource_type_add_another_js',
      'wrapper' => 'resource-type-sub-resources-wrapper',
      'effect' => 'fade',
    ),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save resource type'),
    '#weight' => 40,
  );

  if (!$resource_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete resource type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('resource_type_form_submit_delete'),
    );
  }

  return $form;
}

/**
 * Submit callback to add another sub resource.
 */
function resource_type_add_another_submit($form, &$form_state) {
  $form_state['sub_resource_count']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback to reload the bit of the form with the sub-resources.
 */
function resource_type_add_another_js($form, $form_state) {
  return $form['sub_resources'];
}

/**
 * Check if a sub resource with that name exists.
 */
function resource_type_sub_resource_exists($name) {
  return FALSE;
}

/**
 * Actually validate the machine name.
 */
function resource_type_sub_resource_machine_name_validate($element, &$form_state) {
  $machine_name = $element['#value'];
  $items_parents = $element['#parents'];
  array_pop($items_parents);
  $item_values = drupal_array_get_nested_value($form_state['values'], $items_parents);

  $field = field_info_field('resource_' . $machine_name);

  if (!$field) {
    return;
  }

  $is_entity_reference = ($field['type'] == 'entity_reference');
  $is_resource_reference = ($field['settings']['target_type'] == 'resource');
  $is_type_reference = empty($field['settings']['handler_settings']['target_bundles'])
    || in_array($item_values['type'], $field['settings']['handler_settings']['target_bundles']);
  $cardinality_compatible = ($item_values['cardinality'] == $field['cardinality']);

  if (!($is_entity_reference && $is_resource_reference && $is_type_reference
    && $cardinality_compatible)) {
    form_error($element, t('This machine name is already in use by an incompatible sub resource.'));
  }
}

/**
 * Save submit callback for the resourse_type_form.
 */
function resource_type_form_submit($form, &$form_state) {
  $resource_type = entity_ui_form_submit_build_entity($form, $form_state);

  // Save sub resources.
  $resource_type->resources = array();

  foreach($form_state['values']['sub_resources'] as $key => $values) {
    if ($values['delete']) {
      continue;
    }

    $resource_type->resources[$values['machine_name']] = $values;
  }

  // Save and go back.
  $resource_type->save();
  $form_state['redirect'] = 'admin/structure/resources';
}

/**
 * Delete submit callback for the resource_type_form.
 */
function resource_type_form_submit_delete($form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/resources/manage/' . $form['#resource_type']->type . '/delete';
}
