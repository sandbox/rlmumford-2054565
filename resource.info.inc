<?php
/**
 * @file
 * Contains resource metadata controller class.
 */

class ResourceMetadataController extends EntityDefaultMetadataController {

  /**
   * {@inheritdoc}
   */
  function entityPropertyInfo() {
    $properties = parent::entityPropertyInfo();

    $info = &$properties[$this->type];

    $info['properties']['title']['label'] = t('Title');
    $info['properties']['title']['description'] = t('The title of the resource.');

    $info['properties']['status'] = array(
      'label' => t("Status"),
      'description' => t("Whether the resource is published or unpublished."),
      // Although the status is expected to be boolean, its schema suggests
      // it is an integer, so we follow the schema definition.
      'type' => 'integer',
      'options list' => 'entity_metadata_status_options_list',
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer resources',
      'schema field' => 'status',
    );

    $info['properties']['created']['type'] = 'date';
    $info['properties']['created']['description'] = t('When this resource was created.');

    $info['properties']['changed']['type'] = 'date';
    $info['properties']['changed']['description'] = t('When this resource was last updated.');

    $info['properties']['parent_resource'] = array(
      'label' => t('Parent Resource'),
      'description' => t('The resource this resource is party of.'),
      'getter callback' => 'resource_metadata_get_parent',
      'type' => 'resource',
      'computed' => TRUE,
      'entity views field' => TRUE,
    );

    $info['properties']['child_resources'] = array(
      'label' => t('Sub Resource'),
      'description' => t('The resources that are part of this resource.'),
      'getter callback' => 'resource_metadata_get_children',
      'type' => 'list<resource>',
      'computed' => TRUE,
      'entity views field' => TRUE,
    );

    $info['properties']['edit_link'] = array(
      'label' => t('Edit Link'),
      'description' => t('A link to edit this resource.'),
      'getter callback' => 'resource_metadata_get_link',
      'type' => 'text',
      'computed' => TRUE,
      'entity views field' => TRUE,
    );

    $info['properties']['view_link'] = array(
      'label' => t('View Link'),
      'description' => t('A link to view this resource.'),
      'getter callback' => 'resource_metadata_get_link',
      'type' => 'text',
      'computed' => TRUE,
      'entity views field' => TRUE,
    );

    $info['properties']['delete_link'] = array(
      'label' => t('Delete Link'),
      'description' => t('A link to delete this resource.'),
      'getter callback' => 'resource_metadata_get_link',
      'type' => 'text',
      'computed' => TRUE,
      'entity views field' => TRUE,
    );

    return $properties;
  }
}
