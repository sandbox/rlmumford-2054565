<?php
/**
 * @file
 * Contains ResourceType class.
 */

/**
 * Class for modeling resource types.
 */
class ResourceType extends Entity {

  /**
   * @var int $id
   *   Unique resource type ID.
   */
  public $id;

  /**
   * @var $type
   *   The machine-readable name of this resource type.
   */
  public $type;

  /**
   * @var $label
   *   The human-readable name of this resource type.
   */
  public $label;

  /**
   * @var array $resources
   *   A serialized array of settings for sub resources.
   */
  public $resources = array();

  /**
   * @var array $data
   *   A serialized array of additional data related to this resource type.
   */
  public $data = array();

  /**
   * @var int $status
   *   The exportable status of the entity.
   */
  public $status = ENTITY_CUSTOM;

  /**
   * @var $module
   *   The name of the providing module if the entity has been defined in code.
   */
  public $module;

  /**
   * Returns whether the resource type is locked.
   *
   * Resource types provided in code are automatically treated as locked, as
   * well as any fixed resource type.
   */
  public function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}